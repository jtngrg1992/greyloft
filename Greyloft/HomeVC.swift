//
//  HomeVC.swift
//  Greyloft
//
//  Created by Jatin Garg on 23/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class HomeVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate{
    
    var locationManager = CLLocationManager()
    var center: CLLocationCoordinate2D!
    var infoMarker = GMSMarker()
    var placesClient = GMSPlacesClient()
    var placeImage: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = Globals.accentColor
        tabBarController?.tabBar.barTintColor = Globals.accentColor
        view.backgroundColor = UIColor.white
        navigationItem.title = "Pick Places"
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        loadImage()
    }
    
    func loadImage(){
        let url = "https://d6u22qyv3ngwz.cloudfront.net/ad/Ajfk/the-ups-store-that-place-small-1.jpg"
                URLSession.shared.dataTask(with: NSURL(string: url) as! URL, completionHandler: {(data,response,error) -> Void in
                    if error != nil{
                        let alertVC = UIAlertController(title: "Error", message: "Error Loading image", preferredStyle: .actionSheet)
                        alertVC.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alertVC, animated: true, completion: nil)
                    }
                    else{
                        DispatchQueue.main.async {
                            let image = UIImage(data: data!)
                            self.placeImage = image
                        }
                    }
                }).resume()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error in location manager: " + error.localizedDescription)
    }
    
    func mapView(_ mapView: GMSMapView, didTapPOIWithPlaceID placeID: String, name: String, location: CLLocationCoordinate2D) {
        infoMarker.snippet = placeID
        infoMarker.position = location
        infoMarker.title = name
        infoMarker.opacity = 0;
        infoMarker.infoWindowAnchor = CGPoint(x: 0.44, y: 0.45)
        infoMarker.map = mapView
        infoMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        mapView.selectedMarker = infoMarker
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let v = MView(frame: CGRect(x: 0 , y: 0, width: 200, height: 100))
        v.layer.cornerRadius = 5
        v.layer.masksToBounds = true
        v.placeName.text = marker.title
        //loading image from url
        v.placeImage.image = placeImage
        return v
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude,
                                                          longitude: userLocation!.coordinate.longitude, zoom: 15)
        let mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        self.view = mapView
        
        let marker = GMSMarker()
        marker.position = center
        marker.title = "Current Location"
        marker.snippet = "XXX"
        marker.map = mapView
        
        locationManager.stopUpdatingLocation()

    }
}

class MView: UIView{
    var placeImage: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.contentMode = .scaleAspectFill
        i.clipsToBounds = true
        i.backgroundColor = UIColor.red
        return i
    }()
    
    var placeName: UILabel = {
        let l = UILabel()
        l.adjustsFontSizeToFitWidth = true
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Place Name"
        l.numberOfLines = 0
        return l
    }()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        addSubview(placeName)
        addSubview(placeImage)
        backgroundColor = UIColor.white
        addConstraintWithFormat(format: "H:|-10-[v0(100)]-10-[v1]-10-|", views: placeName,placeImage)
        addConstraintWithFormat(format: "V:[v0(30)]", views: placeName)
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: placeName, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraintWithFormat(format: "V:|-10-[v0]-10-|", views: placeImage)
    }
}
