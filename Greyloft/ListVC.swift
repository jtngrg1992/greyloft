//
//  ListVC.swift
//  Greyloft
//
//  Created by Jatin Garg on 26/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ListVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var imagesToLoad: [Image]!{
        didSet{
            tableView.reloadData()
        }
    }
    var formatter = DateFormatter()
    
    lazy var tableView: UITableView = {
        let t = UITableView()
        t.delegate = self
        t.dataSource = self
        t.translatesAutoresizingMaskIntoConstraints = false
        t.register(TableCell.self, forCellReuseIdentifier: "cell")
        t.tableFooterView = UIView()
        return t
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = Globals.accentColor
        navigationItem.title = "Captured Images"
        view.addSubview(tableView)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: tableView)
        view.addConstraintWithFormat(format: "V:|[v0]|", views: tableView)
        loadImages()
        NotificationCenter.default.addObserver(self, selector: #selector(coreDataChanged), name: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: nil)
    }
    
    func coreDataChanged(notification : Notification){
        let insertedImage = notification.userInfo?[NSInsertedObjectsKey]
        if insertedImage != nil{
            loadImages()
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    func loadImages(){
        if let delegate = UIApplication.shared.delegate as? AppDelegate{
            let context = delegate.persistentContainer.viewContext
            let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Image")
            do{
                imagesToLoad = try context.fetch(fetchReq) as! [Image]
            }catch let e{
                print("Error in fetching \(e)")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = imagesToLoad?.count{
            return count
        }
        return 0
    }
    
    var calender = Calendar.current
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableCell
        if let date = imagesToLoad[indexPath.row].date{
            formatter.dateFormat = "EEEE, MMM d, yyyy"
            let d = formatter.string(from: date as Date)
            cell.cellLabel.text = d
            formatter.timeStyle = .short
            let timeString = formatter.string(from: date as Date)
            cell.timeLabel.text = timeString
            
        }
        
        if let data = imagesToLoad[indexPath.row].data{
            let img = UIImage(data: data as Data)
            cell.cellImage.image = img
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let imgVC = ImageVC()
        imgVC.imageToShow = imagesToLoad[indexPath.row]
        navigationController?.pushViewController(imgVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

class TableCell: UITableViewCell{
    let cellImage: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.layer.cornerRadius = 30
        i.layer.masksToBounds = true
        i.contentMode = .scaleAspectFill
        i.clipsToBounds = true
        return i
    }()
    
    let cellLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Sameple Text"
        l.font = UIFont(name: "AvenirNext-Bold", size: 15)
        l.numberOfLines = 2
        return l
    }()
    
    let timeLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont(name: "AvenirNext-Bold", size: 10)
        l.textColor = UIColor(white: 0.4, alpha: 1)
        l.text = "7:58 PM"
        return l
    }()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        backgroundColor = UIColor(white: 0.95, alpha: 1)
        addSubview(cellImage)
        addSubview(cellLabel)
        addSubview(timeLabel)
        addConstraintWithFormat(format: "H:|-10-[v0(60)]-20-[v1]-10-[v2(50)]-20-|", views: cellImage,cellLabel,timeLabel)
        addConstraintWithFormat(format: "V:[v0(60)]", views: cellImage)
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: cellImage, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: cellLabel, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraintWithFormat(format: "V:|-10-[v0]", views: timeLabel)
    }
}
