//
//  Globals.swift
//  Greyloft
//
//  Created by Jatin Garg on 23/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

class Globals{
    static let controlStoke = UIColor.white
    static let accentColor = UIColor(colorLiteralRed: 247/255, green: 217/255, blue: 93/255, alpha: 1)
    static func getTitleView(with message: String, for navCon: UINavigationController) -> UILabel {
        let l = UILabel(frame: CGRect(x: 0, y: 2, width: navCon.navigationBar.frame.width - 50, height: navCon.navigationBar.frame.height - 10))
        l.font = UIFont(name: "Avenir Next", size: 17)
        l.text = message
        return l
    }
}
