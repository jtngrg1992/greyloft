//
//  LoginVC.swift
//  Greyloft
//
//  Created by Jatin Garg on 23/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func addConstraintWithFormat(format: String, views: UIView...){
        var viewDict = [String: UIView]()
        for (index, view) in views.enumerated(){
            let key = "v\(index)"
            viewDict[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewDict))
    }
}

class LoginVC: UIViewController{
    let emailTF: GTextField = {
        let t = GTextField()
        t.placeholder = "Email"
        return t
    }()
    
    let passwordTF: GTextField = {
        let t = GTextField()
        t.placeholder = "Password"
        t.isSecureTextEntry = true
        return t
    }()
    
    lazy var submitBtn: GButton = {
        let b = GButton()
        b.setTitle("Login", for: .normal)
        b.addTarget(self, action: #selector(submit), for: .touchUpInside)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Globals.accentColor
        view.addSubview(emailTF)
        view.addSubview(passwordTF)
        view.addSubview(submitBtn)
        
        view.addConstraintWithFormat(format: "H:[v0(300)]", views: emailTF)
        view.addConstraint(NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: emailTF, attribute: .centerX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: emailTF, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraintWithFormat(format: "V:[v0(50)]-10-[v1(50)]-10-[v2(50)]", views: emailTF,passwordTF,submitBtn)
        view.addConstraintWithFormat(format: "H:[v0(300)]", views: passwordTF)
        view.addConstraint(NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: passwordTF, attribute: .centerX, multiplier: 1, constant: 0))
        view.addConstraintWithFormat(format: "H:[v0(200)]", views: submitBtn)
        view.addConstraint(NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: submitBtn, attribute: .centerX, multiplier: 1, constant: 0))
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.subviews.forEach { (v) in
            if let textField = v as? GTextField{
                if textField.isEditing{
                    textField.endEditing(true)
                }
            }
        }
    }
    
    func submit(){
        let email = emailTF.text!
        if !isValidEmail(testStr: email){
            emailTF.shake()
            return
        }
        
        if passwordTF.text?.characters.count == 0 {
            passwordTF.shake()
            return
        }
        
        if email.lowercased() == "jatin@jatin.com" && passwordTF.text! == "test"{
            UserDefaults.standard.set(true, forKey: "logged")
            present(TabVC(), animated: true, completion: nil)
        }
        
        else{
            let alert = UIAlertController(title: "Error", message: "The email & password combo entered is incorrect", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

//MARK :- Supplementary Classes

class GTextField: UITextField{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
   required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        backgroundColor = UIColor.black.withAlphaComponent(0.2)
        layer.cornerRadius = 5
        translatesAutoresizingMaskIntoConstraints = false
        layer.masksToBounds = true
        placeholderRect(forBounds: bounds.offsetBy(dx: 2, dy: 2))
        clearButtonMode = .unlessEditing
        clearsOnBeginEditing = true
        textAlignment = .center
        textColor = UIColor.white
        
    }
    
    func shake(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.autoreverses = true
        animation.repeatCount = 5
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 4, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 4, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }

    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: rect.height))
        Globals.controlStoke.setStroke()
        path.lineWidth = 2
        path.addLine(to: CGPoint(x: rect.width, y: rect.height))
        path.stroke()
    }
}

class GButton: UIButton{
    override init(frame: CGRect){
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 5
        layer.masksToBounds = true
        showsTouchWhenHighlighted = true
    }
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 5)
        Globals.controlStoke.setStroke()
        path.lineWidth = 2
        path.stroke()
    }
}
