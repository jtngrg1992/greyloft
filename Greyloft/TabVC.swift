//
//  TabVC.swift
//  Greyloft
//
//  Created by Jatin Garg on 23/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit

class TabVC: UITabBarController{
    override func viewDidLoad() {
        super.viewDidLoad()
        let homeVC = HomeVC()
        let insets = UIEdgeInsetsMake(7, 0, -9, 0)
        let homeNVC = UINavigationController(rootViewController: homeVC)
        homeNVC.tabBarItem.imageInsets = insets
        homeNVC.tabBarItem.image = UIImage(named: "home-tab")
        
        let cameraVC = CameraVC()
        cameraVC.tabBarItem.image =  UIImage(named: "camera")
        cameraVC.tabBarItem.imageInsets = insets
        
        let listVC = ListVC()
        let listNVC = UINavigationController(rootViewController: listVC)
        listNVC.tabBarItem.image = UIImage(named: "list")
        listNVC.tabBarItem.imageInsets = insets
        viewControllers = [homeNVC,cameraVC,listNVC]
    }
}
