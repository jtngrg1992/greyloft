//
//  CameraVc.swift
//  Greyloft
//
//  Created by Jatin Garg on 26/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import CoreData

class CameraVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CAAnimationDelegate{
    var captureSession = AVCaptureSession()
    var stillImageOutput = AVCaptureStillImageOutput()
    var error : NSError? = nil
    var time: Timer!
    var capturedImages = [UIImage]()
    var previewLayer = AVCaptureVideoPreviewLayer()
    var cameraPreview = UIView()
    
    let pickedImage: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.contentMode = .scaleAspectFit
        i.clipsToBounds = true
        return i
    }()
    
    lazy var captueBtn: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.backgroundColor = UIColor.white
        b.layer.cornerRadius = 35
        b.addTarget(self, action: #selector(saveToCam), for: .touchUpInside)
        return b
    }()
    
    lazy var shutterAnimation: CATransition = {
        let c = CATransition()
        c.delegate = self
        c.duration = 0.6
        c.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        c.type = "cameraIris"
        c.setValue("cameraIris", forKey: "cameraIris")
        return c
    }()
    
    lazy var cameraShutter: CALayer = {
        let c = CALayer()
        c.bounds = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        return c
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Globals.accentColor
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if let cameraDevice = device{
            do{
                try captureSession.addInput(AVCaptureDeviceInput(device: cameraDevice))
            }catch let error{
                print("Error adding input device: \(error)")
            }
        }else{
            print("Couldn't unwrap to cameraDevie")
            return
        }
        captureSession.sessionPreset = AVCaptureSessionPresetPhoto
        captureSession.startRunning()
        stillImageOutput.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
        if captureSession.canAddOutput(stillImageOutput){
            captureSession.addOutput(stillImageOutput)
        }else{
            print("Couldnt' add output")
        }
        if let pl = AVCaptureVideoPreviewLayer(session: captureSession){
            previewLayer = pl
            previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            previewLayer.addSublayer(captueBtn.layer)
            cameraPreview.layer.addSublayer(previewLayer)
            view.addSubview(cameraPreview)
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(startCapturing)))
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        previewLayer.bounds = view.bounds
        previewLayer.position = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        cameraPreview = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
        
        captueBtn.frame = CGRect(x: view.frame.midX - 35, y: view.frame.maxY - 150, width: 70, height: 70)
        
        
    }
    
    func startCapturing(){
        view.gestureRecognizers?.forEach({ (gesture) in
            if gesture as? UITapGestureRecognizer != nil{
                self.view.removeGestureRecognizer(gesture)
            }
        })
        
        capturedImages = [UIImage]()
        time = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: {(Timer) -> Void in
            self.saveToCam()
        })
        time.fire()
    }
    func saveToCam(){
        if(capturedImages.count == 4){
            time.invalidate()
            let img = collageImage(rect: CGRect(x: 0, y: 0, width: 1000, height: 1000), images: capturedImages)
            self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(startCapturing)))
            UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
            saveToFS(image: img)
            return
        }
        if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo){
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: {(buffer,error) -> Void in
                if error != nil{
                    print("Failed to capture the image with error: \(error!)")
                }
                else{
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                    self.capturedImages.append(UIImage(data: imageData!)!)
                    self.cameraShutter.bounds = self.view.bounds
                    self.view.layer.addSublayer(self.cameraShutter)
                    self.view.layer.add(self.shutterAnimation, forKey: "cameraIris")
                }})
        }
    }
    
    func saveToFS(image: UIImage){
        if let delegate = UIApplication.shared.delegate as? AppDelegate{
            let container = delegate.persistentContainer
            let context = container.viewContext
            let insertion = NSEntityDescription.insertNewObject(forEntityName: "Image", into: context) as! Image
            insertion.name = "Some Random name"
            insertion.date = NSDate()
            guard let imgData = UIImageJPEGRepresentation(image, 1)
                else{
                    print("JPEG error")
                    return
            }
            insertion.data = imgData as! NSData
            do{
                try context.save()
                print("Save SuccessFull")
            }catch let e{
                print("Error saving to CD: \(e)")
            }
            

        }
    }
    
    
     func collageImage (rect: CGRect, images: [UIImage]) -> UIImage {
        
        let maxImagesPerRow = 2
        var maxSide : CGFloat = 0.0
        
        if images.count >= maxImagesPerRow {
            maxSide = max(rect.width / CGFloat(maxImagesPerRow), rect.height / CGFloat(maxImagesPerRow))
        } else {
            maxSide = max(rect.width / CGFloat(images.count), rect.height / CGFloat(images.count))
        }
        
        var index = 0
        var currentRow = 1
        var xtransform:CGFloat = 0.0
        var ytransform:CGFloat = 0.0
        var smallRect:CGRect = CGRect.zero
        
        var composite: CIImage?
        
        for img in images {
            index += 1
            let x = index % maxImagesPerRow
            
            if x == 0 {
                smallRect = CGRect(x: xtransform, y: ytransform,width: maxSide,height: maxSide)
                currentRow+=1
                xtransform = 0.0
                ytransform = (maxSide * CGFloat(currentRow - 1))
                
            } else {
                smallRect = CGRect(x: xtransform,y: ytransform,width: maxSide,height: maxSide)
                xtransform += CGFloat(maxSide)
            }
            
            var ci = CIImage(image: img)?.applyingOrientation(imageOrientationToTiffOrientation(value: img.imageOrientation))
            ci = ci?.applying(CGAffineTransform(scaleX: maxSide / img.size.width, y: maxSide / img.size.height))
            ci = ci?.applying(CGAffineTransform(translationX: smallRect.origin.x, y: smallRect.origin.y))
            if composite == nil {
                composite = ci
            } else {
                composite = ci?.compositingOverImage(composite!)
            }
        }
        
        let cgIntermediate = CIContext(options: nil).createCGImage(composite!, from: composite!.extent)
        let finalRenderedComposite = UIImage(cgImage: cgIntermediate!)
        
        return finalRenderedComposite
    }
    
    func imageOrientationToTiffOrientation(value: UIImageOrientation) -> Int32
    {
        switch (value)
        {
        case .up:
            return 1
        case .down:
            return 3
        case .left:
            return 8
        case .right:
            return 6
        case .upMirrored:
            return 2
        case .downMirrored:
            return 4
        case .leftMirrored:
            return 5
        case .rightMirrored:
            return 7
        }
    }
}
