//
//  ImageVC.swift
//  Greyloft
//
//  Created by Jatin Garg on 26/11/16.
//  Copyright © 2016 Jatin Garg. All rights reserved.
//

import Foundation
import UIKit
import ImageScrollView

class ImageVC: UIViewController{
    
    var imageScroll: ImageScrollView = {
        let i = ImageScrollView()
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    var imageToShow: Image!{
        didSet{
                addImage()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.hidesBarsOnTap = true
        tabBarController?.tabBar.isHidden = true
        view.backgroundColor = UIColor.black
        view.addSubview(imageScroll)
        view.addConstraintWithFormat(format: "H:|[v0]|", views: imageScroll)
        view.addConstraintWithFormat(format: "V:|[v0]|", views: imageScroll)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageScroll.refresh()
    }
    
    func addImage(){
        DispatchQueue.main.async {
            print("Setting IMage!!")
            self.imageScroll.display(image: UIImage(data: (self.imageToShow?.data)! as Data)!
    )}}}
